#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:2
msgid "A Tiny CDplayer"
msgstr "Un pequeño CDPlayer"

#: Fcdplayer.class:151 .project:1
msgid "CDplayer"
msgstr "CDplayer"

#: Fcdplayer.class:169
msgid "&Eject"
msgstr "&Expulsar"

#: Fcdplayer.class:164
msgid "&Play"
msgstr "&Tocar"

#: Fcdplayer.class:186
msgid "Play &Track"
msgstr "Tocar &Canción"

#: Fcdplayer.class:181
msgid "&Stop"
msgstr "&Parar"

#: Fcdplayer.class:157
msgid "Track"
msgstr "Canción"

#: Fcdplayer.class:196
msgid "Volume"
msgstr "Volumen"

